const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers");
const auth = require("../auth");
const courseController = require("../controllers/courseControllers")

//Route for checking if email exists
router.post("/checkEmail", (req,res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

//Route for User Registration
router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

//Routes for user auth
router.post("/login", (req,res) => { userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

//Route for get user profile
//The "auth.verify" will act as middleware to ensure that the user has logged in before they can get the details
router.post("/details", auth.verify, (req,res)=> {
	const userData = auth.decode(req.headers.authorization);
	console.log(userData)
	userController.getProfile(/*req.body*/{userId:userData.id}).then(resultFromController => res.send(resultFromController))
})


//Route for enrolling the user
router.post("/enroll", auth.verify, (req,res)=>{
	let userData = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	userController.enroll(userData).then(resultFromController => res.send(resultFromController))
});


module.exports = router;