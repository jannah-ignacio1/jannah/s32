const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseControllers");
const auth = require("../auth");


//Route to create a course

router.post("/", auth.verify, (req,res)=>{
	const userData = auth.decode(req.headers.authorization);
	console.log(userData)
	if(userData.isAdmin == true){
	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
} else{
	res.send(`You are not authorized.`)
}
})


/*Activity:
1. Refactor the course route to implement user authentication for the admin when creating a course.
2. Refactor the addCourse controller method to implement admin authentication for creating a course.
3. Push to git with the commit message of Add activity code - S34.
4. Add the link in Boodle.*/

router.get("/all", auth.verify, (req,res)=>{
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
});

/*Mini Activity
Create a route and controller for retrieving all active course. No need to login*/

router.get("/", (req,res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController))
})

//Route for specific course
router.get("/:courseId", (req,res)=>{
	console.log(req.params.courseId)

	courseController.getCourse(req.params).then(resultFromController => {
		res.send(resultFromController)
	})
})

//Route for updating course
router.put("/:courseId", auth.verify, (req,res)=>{
	const data = {
		courseId:req.params.courseId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		updatedCourse: req.body
	}

	courseController.updateCourse(data).then(resultFromController => {res.send(resultFromController)
	})
})

/*Instructions:
1. Create a route for archiving a course. The route must use JWT authentication and obtain the course ID from the url.
2. Create a controller method for archiving a course obtaining the course ID from the request params
3. Process a PUT request at the /courseId/archive route using postman to archive a course
4. Push to git with the commit message of Add activity code - S35.
5. Add the link in Boodle.*/

//Route for archiving course
router.put("/:courseId/archive", auth.verify, (req,res)=>{
	const archiveData = {
		courseId:req.params.courseId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
	}
	console.log(archiveData)
	courseController.archiveCourse(archiveData).then(resultFromController=>{
		res.send(resultFromController)
	})

})
module.exports = router;
