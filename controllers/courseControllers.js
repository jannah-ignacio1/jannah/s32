const Course = require("../models/Course");
//const User = require("../models/User")

module.exports.addCourse = (reqBody) => {
	let newCourse = new Course ({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	return newCourse.save().then((course, error) => {
		if(error) {
			return false
		} else {
			return course
		}
	})
};

//Retrieve all courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result
	})
}

//Retrieve all courses
module.exports.getAllActive = () => {
	return Course.find({isActive:true}).then(result => {
		return result
	})
}

//Controller for specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result =>{
		return result
	})
}


//Updating a course
module.exports.updateCourse = (data) => {
	console.log(data)

	return Course.findById(data.courseId).then((result, error)=>{
		console.log(result);

		if(data.isAdmin){
			result.name=data.updatedCourse.name
			result.description=data.updatedCourse.description
			result.price=data.updatedCourse.price
			//result.isActive=data.updatedCourse.isActive
		
			console.log(result)
			return result.save().then((updatedCourse, error) =>{
				if(error){
					return false
				} else {
					return updatedCourse
				}
				})
		}else{
			return "You are not authorized to update the Course."
		}
	})
}

//Archiving a course
/*module.exports.archiveCourse = (archiveData) => {
	return Course.findById(archiveData.courseId).then((result,error)=>{
		if(archiveData.isAdmin){
			result.isActive=true
		return result.save().then((archivedCourse,alreadyActive)=>{
			if(result.isActive === true){
		return "This course is already archived."
			} else { 
				console.log(archivedCourse)
				return "Course has been archived."
			}
		})
		}/*else if(result.isActive === true){
		return "This course is already archived."
		}else {
		return "You are not an Admin."
	} 
	})
}*/

module.exports.archiveCourse = (archiveData) => {
	return Course.findById(archiveData.courseId).then((result, error)=>{
		if(archiveData.isAdmin == false){
			return "You are not an Admin."
		} if (archiveData.isAdmin && result.isActive == false){
				return "This course has already been archived."
			} else {
				if(archiveData.isAdmin){
					result.isActive = false
					return result.save().then((archivedCourse, error)=>{
						if (error){
							console.log(error)
							return error //("Course ID/User could not be found.") //not working
						} else {
							console.log(archivedCourse)
							return "Course has been archived."
						}
					})
				}
			}
	})
}