const jwt = require("jsonwebtoken")

const secret = "CourseBookingAPI";

//Token Creation
module.exports.createAccessToken = (user) => {
	
	//When the use login, the token will be created with user's information --> THIS IS THE PAYLOAD (JWT token)
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	//Generate a JSON web token using the JWT sign method
	//Generates the token using the form data and the secret code with no additional options provided.
	return jwt.sign(data, secret, {});
}

module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization;

	if(typeof token !== "undefined") {console.log(`${token} Before slice`)

		//to remove the "Bearer", get only the token
		//Syntax: string.slice(start, end)
	token = token.slice(7, token.length);

	console.log(token)

	return jwt.verify(token, secret, (err, data) => { 
		if (err) {
			return res.send({auth:"failed"})
		} else {
			next();
		}
	})
}	else {
	return res.send({auth:"failed"})
}
};

module.exports.decode = (token) => {
	if(typeof token !== "undefined") {
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data) => {
			if (err) {
				return null
			} else {
				return jwt.decode(token, {complete: true}).payload;
			}
		})
	} else {
		return null
	}
}

/*module.exports.verifyAdmin = (req, res, next) => {
	verify(req, res,() => {
		if(req.user.isAdmin){
			next();
		} else {
			return res.send("You are not an Admin.")
		}
	}
}*/