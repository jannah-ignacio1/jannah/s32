//API

//Booking System MVP Requirements


const express = require("express");
const mongoose = require("mongoose");

//Allows our backend application to be available to our frontend application
//Allows to control the app's Cross origin Resource Sharing settings
const cors = require("cors");
const port = 4000;
const userRoutes = require("./routes/userRoutes")
const courseRoutes = require("./routes/courseRoutes")
const app = express();

mongoose.connect("mongodb+srv://admin:123@course.yg49u.mongodb.net/s32-36?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
}) 

/*   OR 
	.then(()=>console.log("Now connected to MongoDB Atlas")).catch((err)=> {console.log(err)});
*/

let db = mongoose.connection;
db.on('error', () => console.error.bind(console, 'Connection Error'));
db.once('open', () => console.log('Now connected to MongoDB Atlas'));

//WILL CONVERT THE TEXTS IN REQ.BODY TO JSON
app.use(express.json());
app.use(express.urlencoded({extended:true}));

//Allows all resouces to access our backend application
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

//will use the defined port number for the application whenever an environment variable is available OR will use port 4000 if non is defined
//This sytanx will allow flexibility when using the application locally or as a hosted application
app.listen(process.env.PORT || port, () => {
	console.log(`API is now online on port ${ process.env.PORT || port}`)
}); 