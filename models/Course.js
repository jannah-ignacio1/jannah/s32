const mongoose = require("mongoose")

const courseSchema = new mongoose.Schema({
	name: {
		type: String,
		//Requires the data for our fields/properties to be included when creating a record
		//The "true" value defines if the field is required or not and the second element in the array is the message that will appear in our terminal when the data is not present
		required: [true, "Course Name is required"]
	},
	description: {
		type: String,
		requried: [true, "Description is required."]
	},
	price: {
		type: Number,
		required: [true, "Price is required."]
	},
	//for archiving of course (e.g. find(isActive:true)) --> if course is already fully-booked, course will be hidden
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	enrollees: [
		{
			userId: {
				type: String,
				required: [true, "UserId is required."]
			},
			enrolledOn: {
				type: Date,
				default: new Date()
			}
		}
	]
});

//"Course" - collection name
module.exports = mongoose.model("Course", courseSchema);